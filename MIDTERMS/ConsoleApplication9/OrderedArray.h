#pragma once
#include <assert.h>

using namespace std;
template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	//getpotision
	// int getPosition(T value)  //get value in mArray
	//{
	//	for (int i = 0; i < mNumElements; i++)
	//	{
	//		if (value < mArray[i])
	//		{
	//			return i; //put new value into right position 
	//		}
	//	}
	//	return mNumElements;
	//}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		//your code goes after this line

		//insertion sort
		int i = mNumElements - 1;      //move of greater

		while (value < mArray[i] && i >= 0)   // compare current element to all its previous elements 
		{                                     // starts at second element
			mArray[i + 1] = mArray[i];     // element move forward
			//i -= 1;                      
			i--;
		}
		mArray[i + 1] = value;    // place element into proper place
		mNumElements++;

	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T & operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	virtual int binarySearch(T val)
	{
		//your code goes after this line

		int low = 0;
		int high = mMaxSize - 1;  //
		int mid;

		// while(low<=high)
		mid = low + (high - low) / 2; 
		for (int i = 0; i < mNumElements; i++)
		{
			if (mArray[i] == mArray[mid])    //inputSearch = starts at middle of array
			{
				return mid;
			}
			else if (mArray[i] > mArray[mid])   //middle element lower, mid+1
			{
				low = mid + 1;       
			}
			else                              //middle element higher, mid-1
			{
				high = mid - 1;
			}

		}
		return -1;
	}

private:
	T * mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T * temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};