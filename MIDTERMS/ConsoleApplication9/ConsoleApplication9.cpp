#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include "UnorderedArray.h"
#include "OrderedArray.h"

using namespace std;

void main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	//OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGenerated array: " << endl;

	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
	{
		cout << unordered[i] << "   ";
	}

	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
	{
		cout << ordered[i] << "   ";
	}
	cout << endl;

	// while loop: 
	while (true)
	{
		// menu:
		cout << endl;
		cout << " [1] = Remove element at index" << endl;
		cout << " [2] = Search for element" << endl;
		cout << " [3] = expand and generate random values" << endl;

		char decision;
		int enterIndex;
		int enterSize;
		int input;

		cout << endl;
		cout << "What do you want to do? " << endl;
		cin >> decision;

		if (decision == '1')
		{
			cout << "Enter index to remove: " << endl;
			cin >> enterIndex;

			ordered.remove(enterIndex);
			unordered.remove(enterIndex);

			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
			{
				cout << unordered[i] << "   ";
			}

			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
			{
				cout << ordered[i] << "   ";
			}
			cout << endl;
		}
		else if (decision == '2')
		{
			cout << "\n\nEnter number to search: ";
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";

			int result = unordered.linearSearch(input);

			if (result >= 0)
			{
				cout << input << " was found at index " << result << ".\n";
			}
			else
			{
				cout << input << " not found." << endl;
			}

			cout << "Ordered Array(Binary Search):\n";

			result = ordered.binarySearch(input);

			if (result >= 0)
			{
				cout << input << " was found at index " << result << ".\n";
			}
			else
			{
				cout << input << " not found." << endl;
			}

		}
		else if (decision == '3')
		{
			cout << "Input size of expansion: " << endl;
			cin >> enterSize;

			for (int i = 0; i < enterSize; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
			{
				cout << unordered[i] << "   ";
			}

			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
			{
				cout << ordered[i] << "   ";
			}
			cout << endl;

		}
		
		system("pause");
	}
}