1.)

#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

void reverseString(string input) {
	if (input.size() == 0) return;
	reverseString(input.substr(1));
	cout << input[0];
}

int main(){
	char inputWord[100];
	cout << "Input word: ";
	cin.getline(inputWord, 100);
	reverseString(inputWord);

	return 0;
}

--------------------------------------------------------------------------------------------------------
2.)

#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

void printArray(int num) {
	if (num > 0) {
		printArray(num - 1);
		cout << num << " ";
	}
	return;
}

void printTriplets(int array[], int list, int numIndexOne, int numIndexTwo, int numIndexThree, int sum) {
	
	//for (int numIndexOne = 0; i < list - 2; numIndexOne++) {
	//	for (int numIndexTwo = numIndexOne; numIndexTwo < list - 1; numIndexTwo++) {
	//		for (int numIndexThree = numIndexTwo + 1; k < list; numIndexThree++) {
	
	if (numIndexOne < list - 2) return printTriplets(array, list, numIndexOne+1, numIndexTwo, numIndexThree, sum);
	else if (numIndexTwo < list - 1) return printTriplets(array, list, numIndexOne, numIndexTwo+1, numIndexThree, sum);
	else if (numIndexThree < list) return printTriplets(array, list, numIndexOne, numIndexTwo, numIndexThree+1, sum);


	if (array[numIndexOne] + array[numIndexTwo] + array[numIndexThree] == sum )
		cout << "Triples found: " << array[numIndexOne] << " ," << array[numIndexTwo] << " ," << array[numIndexThree] << endl;
	else cout << "No Triple found";

}

int main(){
	
	//2
	int arrayOfNumbers[] = { 8,-6,4,2,0,10};
	int numList = sizeof(arrayOfNumbers) / sizeof(arrayOfNumbers[0]);
	int inptSum, indexOne =0 , indexTwo=0, indexThree=0;
	
	for (int i = 0; i < numList; i++) {
		cout << arrayOfNumbers[i] << " ";
}
	cout << endl;
	cout << "Enter sum: ";
	cin >> inptSum;
	printTriplets(arrayOfNumbers, numList, indexOne, indexTwo, indexThree, inptSum);

	return 0;
}


--------------------------------------------------------------------------------------------------------
3.)

#pragma once
#include <assert.h>

using namespace std;
template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	//virtual ~OrderedArray()
	//{
	//	if (mArray != NULL)
	//	{
	//		delete[] mArray;
	//		mArray = NULL;
	//	}
	//}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		//your code goes after this line

		//insertion sort
		int i = mNumElements - 1;      //move of greater

		while (value < mArray[i] && i >= 0)   // compare current element to all its previous elements 
		{                                     // starts at second element
			mArray[i + 1] = mArray[i];     // element move forward
			//i -= 1;                      
			i--;
		}
		mArray[i + 1] = value;    // place element into proper place
		mNumElements++;

	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T & operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}
	
private:
	T * mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T * temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};



#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include"OrderedArray.h"

using namespace std;

int binarySearch(OrderedArray<int> ordered, int inputNum, int min, int max) {
	min = 0;

	// to avoid overflow
	int mid = min + (max - min) / 2;   

	if (ordered[mid] == inputNum) return mid;
	else if (ordered[mid] < inputNum) return (ordered, inputNum, mid + 1, max);
	else return (ordered, inputNum, min, mid - 1);
}


int main(){
	
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	int inputSearch;

	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		ordered.push(rng);
	}

	cout << "Array: " << endl;
	for (int i = 0; i < ordered.getSize(); i++)
	{
		cout << "[ " << ordered[i] << " ]   ";
	}
	cout << endl;

	cout << "What number would you like to search? ";
	cin >> inputSearch;

	int n = sizeof(ordered) / sizeof(ordered[0]);
	int min = 0, max = n - 1;
	int index = binarySearch(ordered, inputSearch, min, max);

	if (index != -1)cout << inputSearch << " is found at index " << index;
	else cout << inputSearch << " is not found";

	return 0;
}