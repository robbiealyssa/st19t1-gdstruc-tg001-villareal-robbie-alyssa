#pragma once
#include <assert.h>


template<class T> //placeholder T - type na pinapasa; T - could be anything
class UnorderedArray
{
public: 
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0),   // // growBy - default value is 1
		mGrowSize(0), mNumElements(0)                              
	{
		if (size)
		{
			mMaxSize = size;

			mArray = new T[mMaxSize];  // type T
			memset(mArray, 0, sizeof(T) * mMaxSize); // allocates memory

			mGrowSize = ((growBy > 0) ? growBy : 0);   // single line if-else statement; Termary operator
			// 
		}
	}

	virtual ~UnorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);    // assert feed condition to assert condition is true continue; condition not true discontinue
		
		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
		{
		
			mNumElements--;
		
		}
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize)
		{
			return;
		}

		for (int i = index; i < mMaxSize - 1; i++)
		{
			mArray[i] = mArray[i + 1];
		}

		mMaxSize--;

		if (mNumElements >= mMaxSize)
		{
			mNumElements = mMaxSize;
		}
	}

private:  
	T * mArray;     // mArray is type T Generiv
	int mMaxSize;     //m private variable
	int mGrowSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T*temp = new T[mMaxSize + mGrowSize];

		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);
		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;

		return true;
	} 
};