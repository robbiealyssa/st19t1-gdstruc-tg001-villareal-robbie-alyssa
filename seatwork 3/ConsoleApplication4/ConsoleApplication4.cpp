#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

void printNumbersReverseFrom(int from)
{
	for (int i = from; i > 0; i--)
	{
		cout << i << " ";
	}
}

void printNumbersReverseRecusively(int from)
{
	if (from <= 0)
	{
		return;
	}
	cout << from << " ";
	printNumbersReverseRecusively(from - 1);
}

int printSumOfInputNumbers(int inputNumbers)
{
	if (inputNumbers != 0)
	{
		cout << inputNumbers % 10 << " + ";
		return  inputNumbers % 10 + printSumOfInputNumbers(inputNumbers / 10);
	}
    else
		return 0;

}

int printFibonacci(int inputNumbers)
{
	if ((inputNumbers == 1 || inputNumbers == 0))
		return inputNumbers;
	else
		return (printFibonacci(inputNumbers - 1) + printFibonacci(inputNumbers - 2));
}

//bool checkIfPrimeOrNot(int inputNumbers, int i=2)
//{
//	if(inputNumbers<=1)
//		return (inputNumbers == )
//	if (inputNumbers % i == 0)
//		return false;
//	if (i * i > inputNumbers)
//		return true;
//	else
//		return checkIfPrimeOrNot(inputNumbers);
//}

int main()
{
	int inputNumbers = 0 ;
	int counter = 0;
	cout << "Input numbers " << endl;
	cin >> inputNumbers;
	cout << " = " << printSumOfInputNumbers(inputNumbers) << endl;

	cout << "Fibonacci: " << endl;
	while (counter < inputNumbers)
	{
		cout << printFibonacci(counter) << endl;
		counter++;
	}
	
	/*cout << "PRime number or not?: ";
	if (checkIfPrimeOrNot(inputNumbers))
		cout << " iss a prime number" << endl;
	else cout << " is not a prime number" << endl;*/

	return 0;
}
