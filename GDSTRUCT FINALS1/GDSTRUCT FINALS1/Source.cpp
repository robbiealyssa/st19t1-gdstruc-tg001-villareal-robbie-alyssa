#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"
#include "UnorderedArray.h"

using namespace std;

void cls() {
	system("pause");
	system("cls");
}

int main() {

	char decision;
	int inputSize;
	int inputNumber;

	cout << "Enter size: ";
	cin >> inputSize;

	cls();

	Queue<int> queue(inputSize);
	Stack <int> stack(inputSize);

	while (true) {

		cout << "What do you want to do? " << endl;
		cout << "[1] = Push Elements" << endl;
		cout << "[2] = Pop Elements" << endl;
		cout << "[3] = Print Everything" << endl;

		cin >> decision;

		cls();

		if (decision == '1') {
			//push/insert
			cout << "Enter number: ";
			cin >> inputNumber;

			cout << "Top element of sets: " << endl;
			// insert code
			queue.push(inputNumber);
			stack.push(inputNumber);

			cout << "Queue: " << queue.front() << endl;
			cout << "Stack:" << stack.top() << endl;

			cls();
		}
		else if (decision == '2') {
			//pop/remove
			cout << "You popped the front elements " << endl;
			// put code
			queue.pop();
			stack.pop();  // popFront 

			cout << "Top element of sets:" << endl;
			cout << "Queue: " << queue.front() << endl;
			cout << "Stack:" << stack.top() << endl;

			system("pause");

			cls();
		}
		else if (decision == '3') {
			//display all nums
			
			cout << "Queue:" << endl;
			queue.displayStack();
			cout << endl;
			cout << "Stack: " << endl;
			stack.displayStack();
			cout << endl;

			system("pause");

			cls();
			// delete the array 
			queue.deleteQueue();
			stack.deleteStack();
		}
	}

	return 0;
}
