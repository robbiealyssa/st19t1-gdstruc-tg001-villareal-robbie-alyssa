#pragma once
#include <assert.h>
#include "UnorderedArray.h"
#include <iostream>

using namespace std;

template<class T>
class Stack {
public:
	Stack(int size) :mArray(NULL) {
		mArray = new UnorderedArray<T>(size);
	}

	virtual void push(T value) {
		mArray->push(value);
	}

	virtual void pop() {
		mArray->pop();
	}

	virtual int getSize()
	{
		return mArray->getSize();
	}

	virtual T& operator[](int index) {
		return mArray[0][index];
	}

	virtual const T& top() {
		return mArray[0][mArray->getSize() - 1];
	}

	virtual void displayStack() {
		
		// reverse
		for (int i = mArray->getSize() - 1; i > 0 - 1; i--) {
			cout << mArray[0][i] << endl;
		}
	}

	virtual void deleteStack() {
		mArray->deleteArray();
	}

private:
	UnorderedArray<T>* mArray;
};
