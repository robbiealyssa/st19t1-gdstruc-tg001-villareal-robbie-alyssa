#pragma once
#include <assert.h>
#include "UnorderedArray.h"
#include <iostream>
using namespace std;

template<class T>
class Queue {
public:
	Queue(int size) :mArray(NULL) {
		mArray = new UnorderedArray<T>(size);
	}

	virtual void push(T value) {
		mArray->push(value);
	}

	virtual void pop() {
		//remove the 1st element
		mArray->remove(0);
		mArray->pop();
	}

	virtual int getSize(){
		return mArray->getSize();
	}

	virtual T& operator[](int index){
		return mArray[0][index];
	}

	virtual const T& front() {
		return mArray[0][0];
	}

	virtual void displayStack() {
		for (int i = 0; i < mArray->getSize(); i++) {
			cout << mArray[0][i] << endl;
		}
	}

	virtual void deleteQueue() {
		mArray->deleteArray();
	}

private:
	UnorderedArray<T>* mArray;
};